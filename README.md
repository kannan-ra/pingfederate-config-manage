## Configuring Ping Federate

#### Configurations

`@ConfigurationProperties` annotation is used to autowire the spring beans from properties file.
Open the `application-dev.properties` file and set your own configurations. Use another file for different environment.

#### Prerequisites

- Java 8
- Maven > 3.0

#### Build and run

Go on the project's root folder, then type:
```
mvn spring-boot:run -Drun.profiles=dev
```

## REST Calls & Idempotency
* If the APIs have option to create resource,
    * If the resource exists in Server,
        * then PUT is always called
        * we will not compare the object before update
        * Reason for this is - PING classes haven't implemented `equals/hashcode`
    * If the resource don't exists in Server, 
        * then POST is called
    * If the resource exists in Server but not in the Application Config properties,
        * then DELETE is called
* When some APIs have only GET/PUT available
    * Application Properties only record the fields that's of interest
    * We start with GET & then compare only the fields we are interested through `compareSelective()`
    * If the state is different in Server, 
        * then PUT is called