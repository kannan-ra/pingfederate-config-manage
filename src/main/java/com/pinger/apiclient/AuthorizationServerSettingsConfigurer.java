package com.pinger.apiclient;

import com.pinger.application.RestTemplateConfig;
import com.pinger.model.PingerAuthorizationServerSettings;
import com.pingidentity.admin.api.model.oauth.AuthorizationServerSettings;
import com.pingidentity.admin.api.model.oauth.ScopeEntry;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@ConfigurationProperties(prefix="host")
@EnableConfigurationProperties({RestTemplateConfig.class, PingerAuthorizationServerSettings.class})
public class AuthorizationServerSettingsConfigurer {

    private static final Logger log = LogManager.getLogger(AuthorizationServerSettingsConfigurer.class.getName());

    private static String API_RESOURCE = "oauth/authServerSettings/";

    @Getter
    @Setter
    String apiBaseUrl;

    @Autowired
    PingerAuthorizationServerSettings config;

    @Autowired
    RestTemplate restTemplate;

    public void configure() {

        AuthorizationServerSettings authorizationServerSettings = restTemplate.getForObject(
                apiBaseUrl + API_RESOURCE,
                AuthorizationServerSettings.class);

        if (!config.compareSelective(authorizationServerSettings)) {
            // update the authorisation server Oauth Scope
            authorizationServerSettings.setDefaultScopeDescription(config.getDefaultScopeDescription());
            List<ScopeEntry> scopeEntryList = new ArrayList<>();
            for (ScopeEntry configScope: config.getScopes()){
                ScopeEntry entry = new ScopeEntry(configScope.getName(), configScope.getDescription());
                scopeEntryList.add(entry);
            }
            authorizationServerSettings.setScopes(scopeEntryList);
            restTemplate.put(apiBaseUrl + API_RESOURCE,
                    authorizationServerSettings);
            log.info("Updated - " + config.toString());
        }
    }
}
