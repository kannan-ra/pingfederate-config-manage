package com.pinger.apiclient;

import com.pinger.application.RestTemplateConfig;
import com.pinger.model.PingerAccessTokenManagers;
import com.pingidentity.admin.api.model.plugin.AccessTokenManager;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@ConfigurationProperties(prefix="host")
@EnableConfigurationProperties({RestTemplateConfig.class, PingerAccessTokenManagers.class})
public class AccessTokenManagersConfigurer {
    private static final Logger log = LogManager.getLogger(AccessTokenManagersConfigurer.class.getName());

    private static String API_RESOURCE = "oauth/accessTokenManagers/";

    @Getter
    @Setter
    String apiBaseUrl;

    @Autowired
    PingerAccessTokenManagers configManagers;

    @Autowired
    RestTemplate restTemplate;

    public void configure() {
        // obtain the server configManagers using GET
        PingerAccessTokenManagers serverManagers = restTemplate.getForObject(
                apiBaseUrl + API_RESOURCE,
                PingerAccessTokenManagers.class);

        // Server Items that are handled - a tracker
        List<AccessTokenManager> trackedServerManagers = new ArrayList<>();

        // Compare the Config Objects with Server Objects
        if (configManagers.getItems() != null) {
            for (AccessTokenManager configItem : configManagers.getItems()) {
                boolean configItemFound = false;
                for (AccessTokenManager serverItem : serverManagers.getItems()) {
                    if (configItem.getId().equals(serverItem.getId())) {
                        // call the PUT to update
                        restTemplate.put(apiBaseUrl + API_RESOURCE + serverItem.getId(),
                                configItem);
                        log.info("Updated - " + configItem.toString());
                        trackedServerManagers.add(serverItem);
                        configItemFound = true;
                        break;
                    }
                }
                if (!configItemFound) {
                    // call POST my configManagers
                    ResponseEntity<AccessTokenManager> responseEntity = restTemplate.postForEntity(
                            apiBaseUrl + API_RESOURCE,
                            configItem,
                            AccessTokenManager.class);
                    log.info(responseEntity.getStatusCode() + " Created - " + configItem.toString());
                }
            }
        }

        // remove the ones that are handled
        serverManagers.getItems().removeAll(trackedServerManagers);

        // loop through remaining items & trigger DELETE
        for (AccessTokenManager serverItem : serverManagers.getItems()) {
            restTemplate.delete(apiBaseUrl + API_RESOURCE + serverItem.getId());
            log.info("Deleted - Item with Id:" + serverItem.getId());
        }
    }
}
