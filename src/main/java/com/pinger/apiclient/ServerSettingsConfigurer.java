package com.pinger.apiclient;

import com.pinger.application.RestTemplateConfig;
import com.pinger.model.PingerServerSettings;
import com.pingidentity.admin.api.model.systemsettings.ServerSettings;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@ConfigurationProperties(prefix="host")
@EnableConfigurationProperties({RestTemplateConfig.class, PingerServerSettings.class})
public class ServerSettingsConfigurer {

    private static final Logger log = LogManager.getLogger(ServerSettingsConfigurer.class.getName());

    private static String API_RESOURCE = "serverSettings/";

    @Getter @Setter
    String apiBaseUrl;

    @Autowired
    PingerServerSettings config;

    @Autowired
    RestTemplate restTemplate;

    public void configure() {

        // obtain the list from the Server with a GET
        ServerSettings serverSettings = restTemplate.getForObject(
                apiBaseUrl + API_RESOURCE,
                ServerSettings.class);

        // Compare the Config with Server Object
        if (! config.compareSelective(serverSettings)) {
            boolean enableOauth = config.getRolesAndProtocols().getOauthRole().isEnableOauth();
            boolean enableOauthOpenId = config.getRolesAndProtocols().getOauthRole().isEnableOpenIdConnect();
            serverSettings.getRolesAndProtocols().getOauthRole().setEnable(enableOauth);
            serverSettings.getRolesAndProtocols().getOauthRole().setEnableOpenIdConnect(enableOauthOpenId);

            restTemplate.put(apiBaseUrl + API_RESOURCE,
                    serverSettings);
            log.info("Updated - " + config.toString());
        }
    }
}
