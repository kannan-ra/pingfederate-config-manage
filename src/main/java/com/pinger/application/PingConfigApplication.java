package com.pinger.application;

import com.pinger.apiclient.AuthorizationServerSettingsConfigurer;
import com.pinger.apiclient.AccessTokenManagersConfigurer;
import com.pinger.apiclient.DataSourcesConfigurer;
import com.pinger.apiclient.ServerSettingsConfigurer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages={
				"com.pinger.apiclient",
				"com.pinger.model",
				"com.pinger.application"})
public class PingConfigApplication {

	private static final Logger log = LogManager.getLogger(PingConfigApplication.class.getName());

	@Autowired
	ServerSettingsConfigurer serverSettingsConfigurer;

	@Autowired
	AuthorizationServerSettingsConfigurer oAuthScopeManageConfigurer;

	@Autowired
	AccessTokenManagersConfigurer oAuthTokenManagersConfigurer;

	@Autowired
	DataSourcesConfigurer dataSourcesConfigurer;

	public static void main(String[] args) {
		SpringApplication.run(PingConfigApplication.class, args);
	}

	@Bean
	public CommandLineRunner run() throws Exception {
		return args -> {
			log.info("Configuring the Server Settings OAuth Role ...");
			serverSettingsConfigurer.configure();

			log.info("Configuring OAuth Scope Management ...");
			oAuthScopeManageConfigurer.configure();

			log.info("Configuring OAuth Access Token Managers ...");
			oAuthTokenManagersConfigurer.configure();

			log.info("Configuring Data Sources ...");
			dataSourcesConfigurer.configure();
		};
	}
}
