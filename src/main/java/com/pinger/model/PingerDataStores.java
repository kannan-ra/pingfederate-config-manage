package com.pinger.model;

import com.pingidentity.admin.api.model.LdapDataStore;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix="serverSettings.dataStores")
@Data
public class PingerDataStores {
    private List<LdapDataStore> items;

    public PingerDataStores(){
    }
}
