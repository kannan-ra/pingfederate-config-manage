package com.pinger.model;

import com.pingidentity.admin.api.model.plugin.AccessTokenManagers;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@EqualsAndHashCode(callSuper = true)
@ConfigurationProperties(prefix = "oauthAccessTokenManagers")
public class PingerAccessTokenManagers extends AccessTokenManagers{

    /*
    default constructor is defined explicitly
    Ref: http://stackoverflow.com/questions/7625783/jsonmappingexception-no-suitable-constructor-found-for-type-simple-type-class
    */
    public PingerAccessTokenManagers(){
        super(null);
    }
}
