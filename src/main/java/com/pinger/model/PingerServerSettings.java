package com.pinger.model;

import com.pingidentity.admin.api.model.systemsettings.ServerSettings;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="serverSettings")
@Data
@EqualsAndHashCode (callSuper = true)
public class PingerServerSettings extends ServerSettings{

    /* beware of difference between lib vs swagger property label
        data-member: enable (lib) vs enableOauth (swagger)
     */

    public PingerServerSettings() {
        super();
    }

    public boolean compareSelective(ServerSettings serverSettings) {
        if (serverSettings.getRolesAndProtocols().getOauthRole().isEnableOauth()
                        == this.getRolesAndProtocols().getOauthRole().isEnableOauth()
                && serverSettings.getRolesAndProtocols().getOauthRole().isEnableOpenIdConnect()
                        == this.getRolesAndProtocols().getOauthRole().isEnableOpenIdConnect())
            return true;
        else
            return false;
    }
}
