package com.pinger.model;

import com.pingidentity.admin.api.model.oauth.AuthorizationServerSettings;
import com.pingidentity.admin.api.model.oauth.ScopeEntry;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="oAuthScopeManagement")
@Data
@EqualsAndHashCode(callSuper = true)
public class PingerAuthorizationServerSettings extends AuthorizationServerSettings {

    public PingerAuthorizationServerSettings() {
        super();
    }

    /* compare only
        * DefaultScopeDescription
        * ScopeEntry List
     */
    public boolean compareSelective(AuthorizationServerSettings settings) {
        if (!this.getDefaultScopeDescription().equals(settings.getDefaultScopeDescription())) {
            return false;
        }
        else {
            // check if all the scope entries are same
            for (ScopeEntry configScope: this.getScopes()) {
                boolean matched =false;
                for (ScopeEntry serverScope: settings.getScopes()) {
                    if (configScope.getName().equals(serverScope.getName())
                            && configScope.getDescription().equals(serverScope.getDescription()))
                        matched = true;
                        break;
                }
                if (!matched)
                    return false;
            }
        }
        return true;
    }
}
